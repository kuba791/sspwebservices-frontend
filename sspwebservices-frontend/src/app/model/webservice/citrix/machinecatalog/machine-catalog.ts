export class MachineCatalog {
  numbersOfVms: number;
  numbersOfProcessors: number;
  memoryInMb: number;
  // domain: string;
  // organizationUnit: string;
  accountNamingSchema: string;
  accountNamingSchemaType: string;
  name: string;
  description: string;
  uid: number;

  constructor(numbersOfVms: number,
              numbersOfProcessors: number,
              memoryInMb: number,
              domain: string,
              organizationUnit: string,
              accountNamingScheme: string,
              accountNamingSchemeType: string,
              name: string,
              machineCatalogDescription: string,
              uid: number) {
    this.numbersOfVms = numbersOfVms;
    this.numbersOfProcessors = numbersOfProcessors;
    this.memoryInMb = memoryInMb;
    // this.domain = domain;
    // this.organizationUnit = organizationUnit;
    this.accountNamingSchema = accountNamingScheme;
    this.accountNamingSchemaType = accountNamingSchemeType;
    this.name = name;
    this.description = machineCatalogDescription;
    this.uid = uid;
  }
}
