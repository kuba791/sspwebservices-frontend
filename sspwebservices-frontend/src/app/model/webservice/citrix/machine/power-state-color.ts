export enum PowerStateColor {
  UNMANAGED = "grey",
  UNKNOWN = "grey",
  UNAVAILABLE = "grey",
  OFF = "red",
  ON = "green",
  SUSPENDED = "yellow",
  TURNING_ON = "green-pulsing",
  TURNING_OFF = "red-pulsing",
  SUSPENDING = "yellow-pulsing",
  RESUMING = "blue-pulsing"
}
