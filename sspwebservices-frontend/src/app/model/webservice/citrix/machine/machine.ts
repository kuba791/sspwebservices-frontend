import {PowerState} from "./power-state";
import {PowerAction} from "./power-action";
import {ActiveDirectoryUser} from "../../../user/active.directory.user";

export class Machine {
  uid: number;
  name: string;
  catalogName: string;
  deliveryGroupName: string;
  assignedUsers: ActiveDirectoryUser[];
  powerState: PowerState;
  supportedPowerActions: PowerAction[];
}
