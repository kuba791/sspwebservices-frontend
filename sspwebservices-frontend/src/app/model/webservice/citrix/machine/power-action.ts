export enum PowerAction {
  RESET = "Reset",
  RESTART = "Restart",
  RESUME = "Resume",
  SHUTDOWN = "ShutDown",
  SUSPEND = "Suspend",
  TURN_OFF = "TurnOff",
  TURN_ON = "TurnOn"
}
