export enum PowerStateTooltip {
  UNMANAGED = "Unmanaged",
  UNKNOWN = "Unknown",
  UNAVAILABLE = "Unavailable",
  OFF = "Off",
  ON = "On",
  SUSPENDED = "Suspended",
  TURNING_ON = "Turning on",
  TURNING_OFF = "Turning off",
  SUSPENDING = "Suspending",
  RESUMING = "Resuming"
}
