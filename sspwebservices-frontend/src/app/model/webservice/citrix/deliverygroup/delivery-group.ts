import {MachineCatalog} from "../machinecatalog/machine-catalog";
import {Machine} from "../machine/machine";

export class DeliveryGroup {
  id: number;
  name: string;
  description: string;
  turnOnMachinesAfterCreation: boolean;
  assignedMachineCatalog: MachineCatalog;
  assignedMachines: Machine[];
}
