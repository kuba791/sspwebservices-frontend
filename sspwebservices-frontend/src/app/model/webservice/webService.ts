export class WebService {
  id: number;
  name: string;
  description: string;
  type: string;
  icon: string;
}
