export interface ActiveDirectoryUser {

  name: string;
  userPrincipalName: string;
  objectGuid: string;
  sid: string;
}
