export class UserPropertyJson {

  userId: number;
  properties = new Map<String, String>();
}
