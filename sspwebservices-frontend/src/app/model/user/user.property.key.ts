export class UserPropertyKey {
  // Constants for ActiveDirectory
  static AD_DOMAIN             = "activedirectory.domain";
  static AD_ORG_UNIT_COMPUTERS = "activedirectory.organizationunit.computers";
  static AD_ORG_UNIT_USERS     = "activedirectory.organizationunit.users";

  // Constants for XenDesktop
  static XENDESKTOP_XD_CONTROLLER_ADMIN_ADDRESS = "xendesktop.xdcontroller.adminaddress";
  static XENDESKTOP_XD_CONTROLLER_CONTROLLERS = "xendesktop.xdcontroller.controllers";
  static XENDESKTOP_STORAGE_RESOURCE = "xendesktop.storage.resource";
  static XENDESKTOP_HOST_RESOURCE = "xendesktop.host.resource";
  static XENDESKTOP_ALLOCATION_TYPE = "xendesktop.allocation.type";
  static XENDESKTOP_PERSIST_CHANGES = "xendesktop.persist.changes";
  static XENDESKTOP_PROVISIONING_TYPE = "xendesktop.provisioning.type";
  static XENDESKTOP_SESSION_SUPPORT = "xendesktop.session.support";
  static XENDESKTOP_MASTER_IMAGE_PATH = "xendesktop.masterimage.path";
}
