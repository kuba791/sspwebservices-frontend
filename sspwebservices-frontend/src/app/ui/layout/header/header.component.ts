import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../service/auth/authentication.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  logoPath = "assets/images/fhswf_logo.png";
  title = "Self-service portal for web services";

  pageTitle: string;

  loginUrl = "login";
  dashboardUrl = "dashboard";
  xendesktopConfigurationUrl = "settings/xendesktop";

  isUserLoggedIn: Observable<boolean>;

  userMail: string;

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.isUserLoggedIn = this.authService.isLoggedIn;
    this.userMail = sessionStorage.getItem("user_mail");
  }

  onLogout() {
    this.authService.logout();
  }

  navigateToXenDesktopConfiguration() {
    this.router.navigateByUrl(this.xendesktopConfigurationUrl).then(r => undefined);
  }

}
