import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserAssignmentDialogComponent} from './user-assignment-dialog.component';

describe('UserAssignmentDialogComponent', () => {
  let component: UserAssignmentDialogComponent;
  let fixture: ComponentFixture<UserAssignmentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAssignmentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAssignmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
