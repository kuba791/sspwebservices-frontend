import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Machine} from "../../../../model/webservice/citrix/machine/machine";
import {ActiveDirectoryUser} from "../../../../model/user/active.directory.user";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {UserAssignmentData} from "../../../wizard/delivery-group-wizard/delivery-group-wizard.component";

@Component({
  selector: 'app-user-assignment-dialog',
  templateUrl: './user-assignment-dialog.component.html',
  styleUrls: ['./user-assignment-dialog.component.css']
})
export class UserAssignmentDialogComponent implements OnInit{

  machine: Machine = this.data.machine;
  users: ActiveDirectoryUser[] = this.data.users;
  selectedUsers: ActiveDirectoryUser[] = this.data.selectedUsers;

  displayedColumns: string[] = ["select", "userName", "userPrincipalName"];
  dataSource = new MatTableDataSource<ActiveDirectoryUser>(this.users);
  selection = new SelectionModel<ActiveDirectoryUser>(true, []);

  result: Map<any, any>;

  constructor(public dialogRef: MatDialogRef<UserAssignmentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UserAssignmentData) {
  }

  ngOnInit() {

    this.result = new Map();

    console.log(this.machine.assignedUsers);
    let assignedUsers: ActiveDirectoryUser[] = this.machine.assignedUsers;

    for (let user of assignedUsers) {
      console.log(user);
    }

    for (let user of this.users) {

      const foundUser = this.machine.assignedUsers.find((assigned) => assigned.userPrincipalName === user.userPrincipalName);
      console.log("Found user: " + foundUser);

      if (foundUser != undefined) {
        this.selection.select(user);
      }
    }
  }

  /**
   * Confirm action
   */
  onConfirmAssignUserToMachine() {

    this.selectedUsers = new Array(this.selection.selected.length);
    for (let user in this.selection.selected) {
      this.selectedUsers[user] = this.selection.selected[user];
    }

    this.result.set("result", true);
    this.result.set("selectedUsers", this.selectedUsers);

    this.dialogRef.close(this.result);
  }

  /**
   * Cancel action
   */
  onCancelAssignUserToMachine() {
    console.log("cancel from dialog called.")

    this.result.set("result", false);

    this.dialogRef.close(this.result);
  }

  /**
   * Whether the number of selected elements matches the total number of rows.
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection.
   */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /**
   * The label for the checkbox on the passed row
   */
  checkboxLabel(row?: ActiveDirectoryUser): string {
    if (!row) {
      return "${this.isAllSelected() ? 'select' : 'deselect'} all";
    }
    return "${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}";
  }
}
