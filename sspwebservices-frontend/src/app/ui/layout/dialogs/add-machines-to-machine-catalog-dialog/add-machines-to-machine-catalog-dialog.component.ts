import {Component, Inject} from '@angular/core';
import {AddMachinesToMachineCatalogData} from "../../../webservice/machines/list-machines.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-add-machines-to-machine-catalog-dialog',
  templateUrl: './add-machines-to-machine-catalog-dialog.component.html',
  styleUrls: ['./add-machines-to-machine-catalog-dialog.component.css']
})
export class AddMachinesToMachineCatalogDialogComponent {

  form: FormGroup;
  machineCatalogNames: string[] = this.data.machineCatalogNames;
  selectedMachineCatalog: string = this.data.machineCatalogNames[0];
  machineCount: number = 1;

  constructor(public dialogRef: MatDialogRef<AddMachinesToMachineCatalogDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: AddMachinesToMachineCatalogData,
              private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      catalogName: [this.selectedMachineCatalog],
      machineCounter: [this.machineCount, [Validators.required, Validators.min(1)]]
    });
  }

  onConfirmAction() {

    const {valid} = this.form;
    this.selectedMachineCatalog = this.form.get("catalogName").value;
    this.machineCount = this.form.get("machineCounter").value;

    if (valid) {

      let result = {
        "selectedMachineCatalog": this.selectedMachineCatalog,
        "machineCount": this.machineCount
      };

      this.dialogRef.close(result);
    }
  }

  logNumbersOfCounts() {
    console.log("Counts: " + this.machineCount);
  }

  onCancel() {
    this.dialogRef.close(null);
  }
}
