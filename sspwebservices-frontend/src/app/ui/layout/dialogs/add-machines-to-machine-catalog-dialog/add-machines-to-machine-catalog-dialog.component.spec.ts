import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMachinesToMachineCatalogDialogComponent } from './add-machines-to-machine-catalog-dialog.component';

describe('AddMachinesToMachineCatalogDialogComponent', () => {
  let component: AddMachinesToMachineCatalogDialogComponent;
  let fixture: ComponentFixture<AddMachinesToMachineCatalogDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMachinesToMachineCatalogDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMachinesToMachineCatalogDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
