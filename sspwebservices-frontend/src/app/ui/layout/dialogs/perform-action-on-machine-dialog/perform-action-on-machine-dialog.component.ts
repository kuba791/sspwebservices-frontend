import {Component, Inject} from '@angular/core';
import {PowerAction} from "../../../../model/webservice/citrix/machine/power-action";
import {Machine} from "../../../../model/webservice/citrix/machine/machine";
import {PerformActionOnMachineData} from "../../../webservice/machines/list-machines.component";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-perform-action-on-machine-dialog',
  templateUrl: './perform-action-on-machine-dialog.component.html',
  styleUrls: ['./perform-action-on-machine-dialog.component.css']
})
export class PerformActionOnMachineDialogComponent {

  constructor(public dialogRef: MatDialogRef<PerformActionOnMachineDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: PerformActionOnMachineData) {
  }

  action: PowerAction = this.data.action;
  machine: Machine = this.data.machine;

  onConfirmAction() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
}
