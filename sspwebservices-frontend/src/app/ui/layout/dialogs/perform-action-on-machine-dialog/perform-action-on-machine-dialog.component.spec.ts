import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformActionOnMachineDialogComponent } from './perform-action-on-machine-dialog.component';

describe('PerformActionOnMachineDialogComponent', () => {
  let component: PerformActionOnMachineDialogComponent;
  let fixture: ComponentFixture<PerformActionOnMachineDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerformActionOnMachineDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformActionOnMachineDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
