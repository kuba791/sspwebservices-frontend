import {Component, Inject} from '@angular/core';
import {Machine} from "../../../../model/webservice/citrix/machine/machine";
import {ActiveDirectoryUser} from "../../../../model/user/active.directory.user";
import {MatTableDataSource} from "@angular/material/table";
import {AssignedUserData} from "../../../webservice/machines/list-machines.component";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-assigned-users-dialog',
  templateUrl: './assigned-users-dialog.component.html',
  styleUrls: ['./assigned-users-dialog.component.css']
})
export class AssignedUsersDialogComponent{

  constructor(public dialogRef: MatDialogRef<AssignedUsersDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: AssignedUserData) {
  }

  machine: Machine = this.data.machine;
  users: ActiveDirectoryUser[] = this.data.users;

  displayedColumns: string[] = ["userPrincipalName"];
  dataSource = new MatTableDataSource<ActiveDirectoryUser>(this.users);

  onCloseDialog() {
    this.dialogRef.close();
  }

}
