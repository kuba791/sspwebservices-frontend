import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMachinesDialogComponent } from './delete-machines-dialog.component';

describe('DeleteMachinesDialogComponent', () => {
  let component: DeleteMachinesDialogComponent;
  let fixture: ComponentFixture<DeleteMachinesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteMachinesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMachinesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
