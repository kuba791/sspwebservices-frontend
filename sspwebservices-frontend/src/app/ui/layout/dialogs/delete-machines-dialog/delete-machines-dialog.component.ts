import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Machine} from "../../../../model/webservice/citrix/machine/machine";
import {DeleteMachinesData} from "../../../webservice/machines/list-machines.component";

@Component({
  selector: 'app-delete-machines-dialog',
  templateUrl: './delete-machines-dialog.component.html',
  styleUrls: ['./delete-machines-dialog.component.css']
})
export class DeleteMachinesDialogComponent implements OnInit {

  machine: Machine = this.data.machine;

  constructor(public dialogRef: MatDialogRef<DeleteMachinesDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DeleteMachinesData) {
  }

  ngOnInit() {
  }


  /**
   * Shows the message for the delete dialog.
   */
  deleteMachinesDialogMessage() {
    // if (this.machines.length > 1) {
    //   return "Do you really want to delete " + this.machines.length + " machines?";
    // }
    // else {
    return "Do you really want to delete the following machine: '" + this.machine.name + "' ?";
    // }
  }

  onConfirmAction() {
    this.dialogRef.close(true);
  }

  onCancel() {
    this.dialogRef.close(false);
  }

}
