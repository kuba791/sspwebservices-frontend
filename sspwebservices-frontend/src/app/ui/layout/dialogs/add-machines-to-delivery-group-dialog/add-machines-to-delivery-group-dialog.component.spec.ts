import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMachinesToDeliveryGroupDialogComponent } from './add-machines-to-delivery-group-dialog.component';

describe('AddMachinesToDeliveryGroupDialogComponent', () => {
  let component: AddMachinesToDeliveryGroupDialogComponent;
  let fixture: ComponentFixture<AddMachinesToDeliveryGroupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMachinesToDeliveryGroupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMachinesToDeliveryGroupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
