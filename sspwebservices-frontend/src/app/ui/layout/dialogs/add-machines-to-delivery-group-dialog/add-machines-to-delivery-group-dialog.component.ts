import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AddMachinesToDeliveryGroupData} from "../../../webservice/machines/list-machines.component";
import {DeliveryGroup} from "../../../../model/webservice/citrix/deliverygroup/delivery-group";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-add-machines-to-delivery-group-dialog',
  templateUrl: './add-machines-to-delivery-group-dialog.component.html',
  styleUrls: ['./add-machines-to-delivery-group-dialog.component.css']
})
export class AddMachinesToDeliveryGroupDialogComponent implements OnInit {

  form: FormGroup;
  deliveryGroups: DeliveryGroup[] = this.data.deliveryGroups;
  selectedDeliveryGroup: DeliveryGroup = this.data.deliveryGroups[0];

  result: Map<any, any>;

  constructor(public dialogRef: MatDialogRef<AddMachinesToDeliveryGroupDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: AddMachinesToDeliveryGroupData, private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      deliveryGroupName: [this.selectedDeliveryGroup] });
  }

  ngOnInit() {
    this.result = new Map();
  }

  onConfirmAction() {
    this.result.set("result", true);
    this.result.set("selectedDeliveryGroup", this.selectedDeliveryGroup);

    this.dialogRef.close(this.result);
  }

  onCancel() {

    this.result.set("result", false);
    this.dialogRef.close(this.result);
  }
}
