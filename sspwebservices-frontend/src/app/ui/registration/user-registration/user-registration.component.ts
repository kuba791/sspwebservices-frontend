import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../service/user/user.service";

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  logoPath = 'assets/images/fhswf_logo.png';

  loginPage = "login";

  userMail = '';
  userPassword = '';
  userPasswordConfirmed = '';

  passwordMatchesConfirmed = true;
  passwordReachedMinLength = true;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  hasPasswordReachedMinLength(): boolean {
    return this.userPassword.length >= 8;
  }

  isConfirmedPasswordMatching(): boolean {
    return this.userPasswordConfirmed === this.userPassword;
  }

  isEnteredInputValid(): boolean {
    return this.hasPasswordReachedMinLength() && this.isConfirmedPasswordMatching();
  }

  onRegisterUser() {
    console.log("registering user.")
    if (this.isEnteredInputValid()) {
      console.log("users input was valid. Registering user now.")
      this.userService.registerUser(this.userMail, this.userPassword);
    }
    else {
      console.log("users input was invalid.")
    }
  }
}
