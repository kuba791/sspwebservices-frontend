import {Component, Injectable, OnInit} from '@angular/core';
import {WebServiceService} from "../../service/webservice/webService.service";
import {WebService} from "../../model/webservice/webService";
import {MachineCatalog} from "../../model/webservice/citrix/machinecatalog/machine-catalog";
import {DeliveryGroup} from "../../model/webservice/citrix/deliverygroup/delivery-group";

const productLogoPathXenDesktop = 'assets/images/ws_citrix_product_logo.png';

@Component({
  selector: 'app-list-web-services',
  templateUrl: './webService.component.html',
  styleUrls: ['./webService.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class WebServiceComponent implements OnInit {

  userId: number;
  webServices: WebService[];

  constructor(public webServiceService: WebServiceService) { }

  ngOnInit() {

    // Init user information
    this.initUserInformation();

    this.getAllAvailableWebServices();
  }

  /**
   * Initializes all needed user information
   */
  initUserInformation() {
    this.userId = Number.parseInt(sessionStorage.getItem('user_id'));
  }

  /**
   * Gets all available web services
   */
  getAllAvailableWebServices() {
    console.log("getAllAvailableWebServices()");

    this.webServiceService.getAllAvailableXenDesktopComponents().subscribe((servicesArray) => {
      this.webServices = servicesArray;
      this.addProductLogoToReceivedWebServices();
    });
  }

  /**
   * Posts a machine catalog to the backend via POST request
   * @param userId ID of current user
   * @param catalog Holds all the information about the catalog to be created.
   */
  orderMachineCatalog(userId: number, catalog: MachineCatalog) {
    console.log("orderMachineCatalog()");
    return this.webServiceService.createMachineCatalog(userId, catalog).subscribe(response => {

      if (response instanceof MachineCatalog){
        return response;
      }
      else {
        return null;
      }
    });
  }

  // ----- Helper methods -----
  /**
   * Sets an icon to each received web service.
   */
  addProductLogoToReceivedWebServices() {
    for (let ws of this.webServices) {
      if (ws.type === "xendesktop_machine_catalog" || ws.type === "xendesktop_delivery_group") {
        ws.icon = productLogoPathXenDesktop;
      }
      else {
        ws.icon = "";
      }
    }
  }

  /**
   * Calls the service to post all information needed to create a delivery group
   * @param userId ID used to assign delivery group to
   * @param deliveryGroup Holds all needed data for creating a delivery group
   */
  createDeliveryGroup(userId: number, deliveryGroup: DeliveryGroup) {
    return this.webServiceService.createDeliveryGroup(userId, deliveryGroup).subscribe(response => {
    });
  }
}
