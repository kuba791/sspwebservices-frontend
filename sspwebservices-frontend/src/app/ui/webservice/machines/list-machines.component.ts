import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebServiceService} from "../../../service/webservice/webService.service";
import {MatDialog} from "@angular/material/dialog";
import {Machine} from "../../../model/webservice/citrix/machine/machine";
import {PowerState} from "../../../model/webservice/citrix/machine/power-state";
import {PowerStateColor} from "../../../model/webservice/citrix/machine/power-state-color";
import {PowerStateTooltip} from "../../../model/webservice/citrix/machine/power-state-tooltip";
import {ActiveDirectoryUser} from "../../../model/user/active.directory.user";
import {MatTableDataSource} from "@angular/material/table";
import {PowerAction} from "../../../model/webservice/citrix/machine/power-action";
import {NotificationService} from "../../../service/notification/notification.service";
import {interval, Subscription} from "rxjs";
import {DataService} from "../../../service/utilities/data.service";
import {SelectionModel} from "@angular/cdk/collections";
import {AddMachinesToMachineCatalogDialogComponent} from "../../layout/dialogs/add-machines-to-machine-catalog-dialog/add-machines-to-machine-catalog-dialog.component";
import {DeleteMachinesDialogComponent} from "../../layout/dialogs/delete-machines-dialog/delete-machines-dialog.component";
import {AssignedUsersDialogComponent} from "../../layout/dialogs/assigned-users-dialog/assigned-users-dialog.component";
import {UserAssignmentDialogComponent} from "../../layout/dialogs/user-assignment-dialog/user-assignment-dialog.component";
import {PerformActionOnMachineDialogComponent} from "../../layout/dialogs/perform-action-on-machine-dialog/perform-action-on-machine-dialog.component";
import {AddMachinesToDeliveryGroupDialogComponent} from "../../layout/dialogs/add-machines-to-delivery-group-dialog/add-machines-to-delivery-group-dialog.component";
import {DeliveryGroup} from "../../../model/webservice/citrix/deliverygroup/delivery-group";

export interface AssignedUserData {
  machine: Machine;
  users: ActiveDirectoryUser[];
}

export interface PerformActionOnMachineData {
  action: PowerAction;
  machine: Machine;
}

export interface DeleteMachinesData {
  machine: Machine;
}

export interface AddMachinesToMachineCatalogData {
  machineCatalogNames: string[];
  machineCount: number;
}

export interface AddMachinesToDeliveryGroupData {
  deliveryGroups: DeliveryGroup[];
}

@Component({
  selector: 'app-list-web-services',
  templateUrl: './list-machines.component.html',
  styleUrls: ['./list-machines.component.css']
})
export class ListMachinesComponent implements OnInit, OnDestroy {

  private updateMachineSubscription: Subscription;

  columnHeaders: string[] = ["name", "deliveryGroupName", "catalogName", "assignedUsers", "powerState", "powerActions"];
  machines: Machine[];

  dataSource = new MatTableDataSource<Machine>(this.machines);
  selection = new SelectionModel<Machine>(true, []);

  // User specific information
  userId: number;
  activeDirectoryUsers: ActiveDirectoryUser[];
  deliveryGroups: DeliveryGroup[];

  dialogResult: boolean;

  constructor(private webServiceService: WebServiceService, public dialog: MatDialog,
              public notificationService: NotificationService, public dataService: DataService) {
  }

  ngOnInit() {
    // Init some user information
    this.initUserInformation();

    // Get all machines of the currently logged in user and refresh it every 30 seconds (for state changing)
    this.retrieveAllMachinesOfUser();
    this.updateMachineSubscription = interval(30000).subscribe(update => {
      this.retrieveAllMachinesOfUser();

    });

    // Get all ActiveDirectory users from currently logged in user
    this.retrieveActiveDirectoryUsersOfCurrentUser();

    // Get all delivery groups of currently logged in user
    this.retrieveAllDeliveryGroupsOfCurrentUser();
  }

  ngOnDestroy(): void {
    this.updateMachineSubscription.unsubscribe();
  }

  /**
   * Retrieves all active directory users from the current user
   */
  retrieveActiveDirectoryUsersOfCurrentUser() {

    this.webServiceService.retrieveActiveDirectoryUsersOfCurrentUser(this.userId).subscribe((users) => {
      this.activeDirectoryUsers = users;
    });
  }

  /**
   * Initializes all needed user information
   */
  initUserInformation() {
    this.userId = Number.parseInt(sessionStorage.getItem('user_id'));
  }

  /**
   * Initializes the data table with all ordered web services of the given user.
   */
  retrieveAllMachinesOfUser() {

    this.webServiceService.retrieveAllMachinesOfUser(this.userId).subscribe((servicesArray) => {
      this.machines = servicesArray;
      this.dataSource = new MatTableDataSource<Machine>(this.machines);
    });
  }

  /**
   * Initializes all available delivery groups of the current user
   */
  retrieveAllDeliveryGroupsOfCurrentUser() {
    this.webServiceService.retrieveAllDeliveryGroupsOfCurrentUser(this.userId).subscribe((deliveryGroups) => {
      this.deliveryGroups = deliveryGroups;
    });
  }

  /**
   * Counts assigned users of given machine.
   * @param machine
   */
  getCountOfAssignedUsersOfMachine(machine: Machine) {
    return machine.assignedUsers.length
  }

  /**
   * Gets the name of the only assigned user, else "-" will be displayed
   * @param machine
   */
  getAssignedUser(machine: Machine) {
    if (machine.assignedUsers.length == 0) {
      return "-";
    }
    else {
      return machine.assignedUsers[0].userPrincipalName;
    }
  }

  /**
   * Shows all assigned users of the selected machine.
   * @param machine
   */
  showAssignedUsers(machine: Machine) {

    this.dialog.open(AssignedUsersDialogComponent, {
      data: {machine: machine, users: machine.assignedUsers}
    });
  }

  /**
   * Needed to decide if count of users will be displayed or users principal name.
   * @param machine
   */
  machineCountGreaterOne(machine: Machine) {
    return machine.assignedUsers.length > 1;
  }

  /**
   * Get the color for the icon to the corresponding power state.
   * @param machine
   */
  getPowerStateColor(machine: Machine) {
    let state: string = PowerState[machine.powerState];
    return "power-state-icon-" + PowerStateColor[state];
  }

  /**
   * Get the tooltip for the icon to the corresponding power state.
   * @param machine
   */
  getPowerStateToolTip(machine: Machine) {
    let state: string = PowerState[machine.powerState];
    return PowerStateTooltip[state];
  }

  /**
   * Adds one or multiple machines to a selected machine catalog.
   */
  onAddNewMachines() {

    // Get all available machine catalog names
    let machineCatalogNames: string[] = this.machines.map(machine => (machine.catalogName));
    let uniqueCatalogNames: string[] = machineCatalogNames.filter(function (item, pos) {
      return machineCatalogNames.indexOf(item) == pos;
    })

    let machineCatalogName: string = "";
    let count: number = 0;

    this.dialog.open(AddMachinesToMachineCatalogDialogComponent, {
      data: {machineCatalogNames: uniqueCatalogNames}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {

        machineCatalogName = resultOfDialog.selectedMachineCatalog;
        count = resultOfDialog.machineCount;

        this.webServiceService.addNewMachinesToMachineCatalog(this.userId, machineCatalogName, count).subscribe(
          (result) => {
            if (result.result > 0) {
              this.notificationService.displaySuccessSnack("Adding machine(s) to the selected machine catalog was successful. This process may take a while. Please, be patient");
            } else {
              this.notificationService.displayErrorSnack("Adding machine(s) failed due to: " + result.message);
            }
          });
      }
    });
  }

  /**
   * Adds one machine to a selected delivery group.
   */
  onAddMachinesToDeliveryGroup(machine: Machine) {
    this.dialog.open(AddMachinesToDeliveryGroupDialogComponent, {
      data: {deliveryGroups: this.deliveryGroups}
    }).afterClosed().subscribe((result) => {

      let currentResult: Map<any, any> = result;

      if (currentResult.get("result")) {

        let tmpMachines: Machine[] = [];
        let tmpMachine: Machine = machine;
        let deliveryGroup: DeliveryGroup = currentResult.get("selectedDeliveryGroup");

        tmpMachine.deliveryGroupName = deliveryGroup.name;

        tmpMachines.push(tmpMachine);

        this.webServiceService.addMachinesToDeliveryGroup(this.userId, tmpMachines).subscribe((result) => {

          console.log("Result of adding machines to DG: " + result.result);

          if (result.result === 0) {
            this.notificationService.displaySuccessSnack("Adding machine(s) to delivery group was successful.");
          } else {
            this.notificationService.displayErrorSnack("Adding machine(s) to delivery group failed due to: " + result.message);
          }
        });
      }
    });
  }

  /**
   * Deletes the selected machines.
   */
  onDelete(machine: Machine) {
    this.dialog.open(DeleteMachinesDialogComponent, {
      data: {machine: machine}
    }).afterClosed().subscribe(result => {

      if (result) {

        let machines: Machine[] = [];
        machines.push(machine);

        this.notificationService.displayInfoSnack("This task can take a while. Please, be patient.");

        this.webServiceService.deleteSelectedMachines(this.userId, machines).subscribe((result) => {
          console.log("Result was: " + result.result);

          // Result returns the corresponding machine catalog uid which has to be greater 0
          if (result.result >= 0) {
            this.notificationService.displaySuccessSnack("Deletion of machines was successful and will be removed from your list in a moment.");
          } else {
            this.notificationService.displayErrorSnack("Deletion failed: " + result.message);
          }
        });
      }
    });
  }

  /**
   * Re-assigns Active directory users to selected machine
   * @param machine
   */
  onAssignUsersToMachines(machine: Machine) {

    this.dialog.open(UserAssignmentDialogComponent, {
      data: {machine: machine, users: this.activeDirectoryUsers}
    }).afterClosed().subscribe(result => {

      let currentResult: Map<any, any> = result;

      if (currentResult.get("result")) {
        let tmpMachine: Machine = machine;
        tmpMachine.assignedUsers = [];
        let selectedUser: Array<ActiveDirectoryUser> = currentResult.get("selectedUsers");
        selectedUser.forEach((user: ActiveDirectoryUser) => tmpMachine.assignedUsers.push(user));

        let machines: Machine[] = [];
        machines.push(tmpMachine);

        this.webServiceService.assignUsersToMachine(this.userId, machines).subscribe((result) => {
          if (result != null) {
            if (result.result === 0) {
              this.notificationService.displaySuccessSnack("User assignment was successful");
            } else {
              this.notificationService.displayErrorSnack("Assigning users to machine failed due to: " + result.message);
            }
          }
        });
      }
    });
  }

  // ----- POWER ACTIONS -----
  /**
   * After confirming the action dialog, a POST-request will be called to RESET the selected machine.
   * @param machine
   */
  performActionReset(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {machine: machine, action: PowerAction.RESET}
    }).afterClosed().subscribe(resultOfDialog => {

      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.RESET)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }

  /**
   * After confirming the action dialog, a POST-request will be called to RESTART the selected machine.
   * @param machine
   */
  performActionRestart(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {userId: this.userId, machine: machine, action: PowerAction.RESTART}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.RESTART)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
         });
      }
    });
  }

  /**
   * After confirming the action dialog, a POST-request will be called to RESUME the selected machine.
   * @param machine
   */
  performActionResume(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {machine: machine, action: PowerAction.RESUME}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.RESUME)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }

  /**
   * After confirming the action dialog, a POST-request will be called to SHUTDOWN the selected machine.
   * @param machine
   */
  performActionShutdown(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {userId: this.userId, machine: machine, action: PowerAction.SHUTDOWN}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.SHUTDOWN)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }

  performActionSuspend(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {userId: this.userId, machine: machine, action: PowerAction.SUSPEND}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.SUSPEND)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }

  /**
   * After confirming the action dialog, a POST-request will be called to TURN_OFF the selected machine.
   * @param machine
   */
  performActionTurnOff(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {userId: this.userId, machine: machine, action: PowerAction.TURN_OFF}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.TURN_OFF)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }

  /**
   * After confirming the action dialog, a POST-request will be called to TURN_ON the selected machine.
   * @param machine
   */
  performActionTurnOn(machine: Machine) {
    this.dialog.open(PerformActionOnMachineDialogComponent, {
      data: {userId: this.userId, machine: machine, action: PowerAction.TURN_ON}
    }).afterClosed().subscribe(resultOfDialog => {
      if (resultOfDialog) {
        this.webServiceService.performActionOnMachine(this.userId, machine.name, PowerAction.TURN_ON)
          .subscribe((result) => {
            if (result != null) {
              if (result.result === 0) {
                this.notificationService.displaySuccessSnack("Action was successful. '" + machine.name + "' will be reset in a moment.");
              } else {
                this.notificationService.displayErrorSnack("Action failed due to: " + result.message);
              }
            }
          });
      }
    });
  }
}
