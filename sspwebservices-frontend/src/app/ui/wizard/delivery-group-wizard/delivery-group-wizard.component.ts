import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WebServiceComponent} from "../../webservice/webService.component";
import {WebServiceService} from "../../../service/webservice/webService.service";
import {ActiveDirectoryUser} from "../../../model/user/active.directory.user";
import {MachineCatalog} from "../../../model/webservice/citrix/machinecatalog/machine-catalog";
import {Machine} from "../../../model/webservice/citrix/machine/machine";
import {MatDialog} from "@angular/material/dialog";
import {SelectionModel} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material/table";
import {DeliveryGroup} from "../../../model/webservice/citrix/deliverygroup/delivery-group";
import {DataService, NavigationResult} from "../../../service/utilities/data.service";
import {UserAssignmentDialogComponent} from "../../layout/dialogs/user-assignment-dialog/user-assignment-dialog.component";

export interface UserAssignmentData {
  machine: Machine;
  users: ActiveDirectoryUser[];
  selectedUsers: ActiveDirectoryUser[];
}

@Component({
  selector: 'app-delivery-group-wizard',
  templateUrl: './delivery-group-wizard.component.html',
  styleUrls: ['./delivery-group-wizard.component.css']
})

export class DeliveryGroupWizardComponent implements OnInit {

  // User specific information
  userId: number;
  activeDirectoryUsers: ActiveDirectoryUser[];

  // selectedActiveDirectoryUsers: ActiveDirectoryUser[];

  detailsGroup: FormGroup;

  // Delivery group details - step 1
  deliveryGroupId: number;
  deliveryGroupName: string;
  deliveryGroupDescription: string;
  machineCatalogs: MachineCatalog[];
  selectedCatalog: MachineCatalog;
  turnOnAddedMachines: boolean;

  // User assignment - step 2
  displayedColumns: string[] = ["select", "name", "assignedUsers", "assignmentAction"];
  dataSource: MatTableDataSource<Machine>;
  columnsToDisplay: string[] = this.displayedColumns.slice();
  machines: Machine[];
  machineSelection = new SelectionModel<Machine>(true, []);
  availableMachineCount: number;
  selectedMachinesCount: number;

  deliveryGroup: DeliveryGroup = {
    id: this.deliveryGroupId,
    name: this.deliveryGroupName,
    description: this.deliveryGroupDescription,
    turnOnMachinesAfterCreation: this.turnOnAddedMachines,
    assignedMachineCatalog: this.selectedCatalog,
    assignedMachines: this.machines
  }

  navigationResult: NavigationResult;

  constructor(private formBuilder: FormBuilder, private webServiceComponent: WebServiceComponent,
              private webServiceService: WebServiceService, private dialog: MatDialog, public dataService: DataService) {
  }

  ngOnInit() {
    // Get user information
    this.initUserInformation();

    // Retrieve all machine catalogs of current user
    this.retrieveAllMachineCatalogsOfCurrentUser();

    // Retrieve all active directory users of the user as well
    this.retrieveActiveDirectoryUsersOfCurrentUser();

    // Initialize form group controls from step 1
    this.detailsGroup = this.formBuilder.group({
      nameController: ["", Validators.required],
      descriptionController: [],
      turnOnAfterCreationController: [false]
    })
  }

  /**
   * Initializes all needed user information
   */
  initUserInformation() {
    this.userId = Number.parseInt(sessionStorage.getItem('user_id'));
  }

  /**
   * Retrieves all machine catalogs of the current user
   */
  retrieveAllMachineCatalogsOfCurrentUser() {

    this.webServiceService.retrieveAllMachineCatalogsOfCurrentUser(this.userId).subscribe((catalogs) => {
      this.machineCatalogs = catalogs;
      this.selectedCatalog = catalogs[0];
    });
  }

  /**
   * Retrieves all active directory users from the current user
   */
  retrieveActiveDirectoryUsersOfCurrentUser() {

    this.webServiceService.retrieveActiveDirectoryUsersOfCurrentUser(this.userId).subscribe((users) => {
      this.activeDirectoryUsers = users;
    });
  }

  /**
   * Gets all available machines of the selected catalog
   */
  getAvailableMachinesOfSelectedCatalog() {

    this.webServiceService.retrieveAllAvailableMachinesOfCatalog(this.userId, this.selectedCatalog.uid).subscribe(
      (catalogMachines) => {
        console.log("selected catalog: " + this.selectedCatalog.name);
        this.machines = catalogMachines;
        this.dataSource = new MatTableDataSource<Machine>(this.machines);
        this.availableMachineCount = this.machines.length;
    })
  }

  /**
   * Creates delivery group and finishes wizard process
   */
  createDeliveryGroup() {
    // Details about the delivery group
    this.deliveryGroupName = this.detailsGroup.get("nameController").value;
    this.deliveryGroupDescription = this.detailsGroup.get("descriptionController").value;
    this.turnOnAddedMachines = this.detailsGroup.get("turnOnAfterCreationController").value;

    // Generate delivery group
    this.deliveryGroup.name = this.deliveryGroupName;
    this.deliveryGroup.description = this.deliveryGroupDescription;
    this.deliveryGroup.turnOnMachinesAfterCreation = this.turnOnAddedMachines;
    this.deliveryGroup.assignedMachineCatalog = this.selectedCatalog;
    this.deliveryGroup.assignedMachines = this.machineSelection.selected;

    // TEST the delivery group
    // console.log("New delivery group:");
    // console.log("Name: " + this.deliveryGroup.name);
    // console.log("Description: " + this.deliveryGroup.description);
    // console.log("Turn on machines after creation: " + this.deliveryGroup.turnOnMachinesAfterCreation);
    // console.log("-----");
    // console.log("Assigned catalog info:");
    // console.log("Catalog name: " + this.deliveryGroup.assignedMachineCatalog.name);
    // console.log("Catalog UID: " + this.deliveryGroup.assignedMachineCatalog.uid);
    // console.log("-----")
    // for (let machine in this.deliveryGroup.assignedMachines) {
    //   console.log("Machine name: " + this.deliveryGroup.assignedMachines[machine].name);
    //   console.log("Current power state: " + this.deliveryGroup.assignedMachines[machine].powerState);
    //   console.log("Assigned users:");
    //   for (let user in this.deliveryGroup.assignedMachines[machine].assignedUsers) {
    //     console.log(this.deliveryGroup.assignedMachines[machine].assignedUsers[user].name);
    //     console.log(this.deliveryGroup.assignedMachines[machine].assignedUsers[user].sid);
    //   }
    //   console.log("Supported power actions:");
    //   for (let action in this.deliveryGroup.assignedMachines[machine].supportedPowerActions) {
    //     console.log(this.deliveryGroup.assignedMachines[machine].supportedPowerActions[action]);
    //   }
    // }

    // POST the delivery group to backend
    let result = this.webServiceService.createDeliveryGroup(this.userId, this.deliveryGroup);

    // Navigates the user back to the dashboard with a positive or negative result
    let message: string;
    let success: boolean = result != null;
    if (success) {
      message = "The delivery group creation is now being processed in the background. Please, be patient.";
    }
    else {
      message = "Delivery group creation failed."
    }

    this.navigationResult.success = success;
    this.navigationResult.message = message;

    this.dataService.navigationResult = this.navigationResult;

    this.webServiceService.navigateToDashboard();
  }

  onAssignUserToMachine(machine: Machine) {
    const dialogRef = this.dialog.open(UserAssignmentDialogComponent, {
      data: {machine: machine, users: this.activeDirectoryUsers}
    });
  }

  // ----- Methods need in the UI -----
  getAllAssignedUsersOfMachine(machine: Machine) {
    if (machine.assignedUsers === null || machine.assignedUsers.length == 0) {
      return "-";
    }
    else {
      let usersString = [];
      for (let user in machine.assignedUsers) {
        usersString[user] = machine.assignedUsers[user].userPrincipalName;
      }
      return usersString.toString();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.machineSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.machineSelection.clear() :
      this.dataSource.data.forEach(row => this.machineSelection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ActiveDirectoryUser): string {
    if (!row) {
      return "${this.isAllSelected() ? 'select' : 'deselect'} all";
    }
    return "${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}";
  }
}
