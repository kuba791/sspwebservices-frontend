import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryGroupWizardComponent } from './delivery-group-wizard.component';

describe('DeliveryGroupWizardComponent', () => {
  let component: DeliveryGroupWizardComponent;
  let fixture: ComponentFixture<DeliveryGroupWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryGroupWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryGroupWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
