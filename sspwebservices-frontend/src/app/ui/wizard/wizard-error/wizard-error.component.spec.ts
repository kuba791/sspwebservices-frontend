import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardErrorComponent } from './wizard-error.component';

describe('WizardErrorComponent', () => {
  let component: WizardErrorComponent;
  let fixture: ComponentFixture<WizardErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
