import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineCatalogWizardComponent } from './machine-catalog-wizard.component';

describe('MachineCatalogWizardComponent', () => {
  let component: MachineCatalogWizardComponent;
  let fixture: ComponentFixture<MachineCatalogWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineCatalogWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineCatalogWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
