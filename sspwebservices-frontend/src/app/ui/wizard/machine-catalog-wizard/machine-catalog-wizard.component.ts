import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MachineCatalog} from "../../../model/webservice/citrix/machinecatalog/machine-catalog";
import {WebServiceComponent} from "../../webservice/webService.component";
import {WebServiceService} from "../../../service/webservice/webService.service";
import {MatStepper} from "@angular/material/stepper";
import {DataService, NavigationResult} from "../../../service/utilities/data.service";

// Constants for the virtual desktop wizard
const MIN_VMS = 1;
const MIN_CPUS = 1;
const MAX_CPUS = 8;
const MIN_RAM = 1;
const MAX_RAM = 16;

const NAMING_SCHEME_TYPE_NUMERIC = 'numeric';
const NAMING_SCHEME_TYPE_ALPHABETIC = 'alphabetic';

interface PerformanceLevel {
  level: string;
  viewLevel: string;
  levelDescription: string;
  cpus: number,
  vram: number,
  checked: boolean;
}

interface AccountNamingSchemeType {
  type: string;
  viewType: string;
}

@Component({
  selector: 'app-machine-catalog-wizard',
  templateUrl: './machine-catalog-wizard.component.html',
  styleUrls: ['./machine-catalog-wizard.component.css']
})
export class MachineCatalogWizardComponent implements OnInit, OnDestroy {

  // User specific information
  userId: number;

  // Form controls
  performanceGroup: FormGroup;
  hardwareGroup: FormGroup;
  machineCatalogGroup: FormGroup;

  form: FormArray;

  // Members
  performanceLevels: PerformanceLevel[] = [
    {level: "low", viewLevel: "Low-Performance", levelDescription: "Suitable for common activities such as working in Office products, browsing, etc.", cpus: 1, vram: 1, checked: true},
    {level: "mid", viewLevel: "Mid-Performance", levelDescription: "Suitable for everyday tasks.", cpus: 2, vram: 2, checked: false},
    {level: "high", viewLevel: "High-Performance", levelDescription: "Suitable for software development, video editing, etc.", cpus: 4, vram: 4, checked: false},
    {level: "custom", viewLevel: "Custom", levelDescription: "Customize your needed hardware performance in the next step.", cpus: 0, vram: 0, checked: false}
  ];

  minVms = MIN_VMS;

  minCpus = MIN_CPUS;
  maxCpus = MAX_CPUS;

  minMem = MIN_RAM;
  maxMem = MAX_RAM;

  // Performance details
  customPerformance: string;
  selectedPerformance: string;

  /* Hardware details */
  public numOfVms: number;
  public numOfCpus: number;
  public memory: number;

  /* Machine catalog details*/
  public accountNamingSchema: string;
  public accountNamingSchemaType: string;
  public name: string;
  public description: string;
  public accountNamingSchemeExample: string;
  public uid: number;
  // public adOrgUnit: string;

  accountNamingSchemeTypes: AccountNamingSchemeType[] = [
    {type: 'numeric', viewType: 'Numeric'},
    {type: 'alphabetic', viewType: 'Alphabetic'}
  ];

  schemeTypeViewValue: string;

  @Input()
  notifyNamingSchemeExample: () => void;

  machineCatalog: MachineCatalog = {
    accountNamingSchema: this.accountNamingSchema,
    accountNamingSchemaType: this.accountNamingSchemaType,
    // domain: null,
    // organizationUnit: this.adOrgUnit,
    description: this.description,
    name: this.name,
    memoryInMb: this.memory * 1024,
    numbersOfProcessors: this.numOfCpus,
    numbersOfVms: this.numOfVms,
    uid: this.uid
  };

  navigationResult: NavigationResult = {
    showSnack: false,
    success: false,
    message: ""
  }

  // Constructor
  constructor(private formBuilder: FormBuilder, private webServiceComponent: WebServiceComponent,
              private webServiceService: WebServiceService, public dataService: DataService) {
  }

  /**
   * Init
   */
  ngOnInit() {

    // Initialize user information
    this.initUserInformation();

    this.performanceGroup = this.formBuilder.group( {
      vmAmountController: [1, Validators.required]
    })

    this.machineCatalogGroup = this.formBuilder.group({
      catalogNameController: ['', Validators.required],
      catalogDescriptionController: [''],
      namingSchemeController: ['', Validators.required],
      namingSchemeTypeController: ['numeric']
      // adOrgUnitController: ['', Validators.required]
    });

    // Pre-select first item of performance step
    this.selectedPerformance = this.performanceLevels[0].level;

    // Init min and max sizes for hardware details
    this.numOfVms = MIN_VMS;
    this.numOfCpus = MIN_CPUS;
    this.memory = MIN_RAM;
  }

  ngOnDestroy() {
    console.log("Navigation result:");
    console.log("Success: " + this.navigationResult.success);
    console.log("Message: " + this.navigationResult.message);

    this.dataService.navigationResult = this.navigationResult;
  }

  /**
   * Initializes all needed user information
   */
  initUserInformation() {
    this.userId = Number.parseInt(sessionStorage.getItem('user_id'));
  }

  /**
   * Selects the performance if clicked on panel around the radio button
   * @param performance
   */
  selectPerformanceOnPanelClick(performance: PerformanceLevel) {
    this.selectedPerformance = performance.level;

    this.setPerformanceToMachineCatalog();
  }

  /**
   * Flag which indicates whether the 'hardware' step should be rendered or not.
   */
  customHardwareSelected():boolean {
    return this.selectedPerformance == "custom";
  }

  /**
   * Sets the selected values to the machine catalog to be created.
   */
  setPerformanceToMachineCatalog() {

    if (this.selectedPerformance != "custom") {
      let performanceLevel = this.performanceLevels.find(level => level.level == this.selectedPerformance);

      this.memory = performanceLevel.vram;
      this.numOfCpus = performanceLevel.cpus;
    }
    else {
      this.memory = 1;
      this.numOfCpus = 1;
    }
  }

  /**
   * Confirms the order of the webservice and transfers information to the spring boot application
   */
  onCreatingMachineCatalog() {

    // Generate new virtual desktop for POSTing it to the backend
    this.generateMachineCatalog();

    // POST the virtual desktop to the backend
    let result = this.webServiceComponent.orderMachineCatalog(this.userId, this.machineCatalog);

    // Navigates the user back to the dashboard with a positive or negative result
    let message: string;
    let success: boolean = result != null;
    if (success) {
      message = "The machine catalog creation will now be processd in the background and will take up to 20 minutes. Please be patient.";
    }
    else {
      message = "Machine catalog creation failed."
    }

    this.navigationResult.showSnack = true;
    this.navigationResult.success = success;
    this.navigationResult.message = message;

    this.dataService.navigationResult = this.navigationResult;

    this.webServiceService.navigateToDashboard();
  }

  /**
   * Generates a json formatted machine catalog out of the entered details from the UI.
   */
  generateMachineCatalog() {

    this.machineCatalog.uid = null;
    this.machineCatalog.accountNamingSchema = this.accountNamingSchema;
    this.machineCatalog.accountNamingSchemaType = this.accountNamingSchemaType.toUpperCase();
    this.machineCatalog.description = this.description;
    this.machineCatalog.name = this.name;
    this.machineCatalog.memoryInMb = this.memory * 1024;
    this.machineCatalog.numbersOfProcessors = this.numOfCpus;
    this.machineCatalog.numbersOfVms = this.numOfVms;
  }

  /**
   * Formats the text in the thumb label of the memory slider
   * @param value
   */
  formatRamSizeLabel(value: number) {
    return value + 'GB';
  }

  /**
   * Creates the account naming scheme example seen in the second step of the wizard.
   * @param event
   */
  onKey(event: any) {

    this.accountNamingSchema = this.machineCatalogGroup.get('namingSchemeController').value;
    this.accountNamingSchemaType = this.machineCatalogGroup.get('namingSchemeTypeController').value;

    this.accountNamingSchemeExample = this.accountNamingSchema;

    if (this.accountNamingSchemeExample.includes('#')) {

      // Replace '#' by selecting naming scheme type to '0' or 'A'
      if (this.accountNamingSchemaType == NAMING_SCHEME_TYPE_NUMERIC) {
        this.accountNamingSchemeExample = this.replaceHashTag(this.accountNamingSchemeExample, false);
      }
      else if (this.accountNamingSchemaType == NAMING_SCHEME_TYPE_ALPHABETIC) {
        this.accountNamingSchemeExample = this.replaceHashTag(this.accountNamingSchemeExample, true);
      }
    }
  }

  /**
   * Replaces all occurrences with either an alphabetic or a numeric char series.
   * @param originalString
   * @param alphabetic
   */
  replaceHashTag(originalString, alphabetic): string {

    // Initialize variable for numeric selection
    let replacer = 0;
    if (alphabetic)
      replacer = 65;

    // Iterate over all chars to identify hash tag
    for (let i = 0; i < originalString.length; i++) {

      if (originalString[i] === '#') {
        // Replace hash tag with alphabetic char e.g. 'A', 'B', 'C'...
        if (alphabetic) {
          originalString = originalString.replace('#', String.fromCharCode(replacer));
        }
        // Replace hash tag with numeric char e.g. '0', '1', '2'...
        else {
          originalString = originalString.replace('#', String(replacer));
        }
        replacer++;
      }
    }
    return originalString;
  }

  /**
   * Sets the values from the control group to the intended variables.
   */
  setMachineCatalogDetails() {

    this.numOfVms = this.performanceGroup.get('vmAmountController').value;

    this.name = this.machineCatalogGroup.get('catalogNameController').value;
    this.description = this.machineCatalogGroup.get('catalogDescriptionController').value;
    this.accountNamingSchema = this.machineCatalogGroup.get('namingSchemeController').value;
    this.accountNamingSchemaType = this.machineCatalogGroup.get('namingSchemeTypeController').value;

    this.schemeTypeViewValue = this.accountNamingSchemaType;

    // this.adOrgUnit = this.machineCatalogGroup.get('adOrgUnitController').value;
  }
}
