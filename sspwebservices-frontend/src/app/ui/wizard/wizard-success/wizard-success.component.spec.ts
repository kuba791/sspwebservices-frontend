import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardSuccessComponent } from './wizard-success.component';

describe('WizardSuccessComponent', () => {
  let component: WizardSuccessComponent;
  let fixture: ComponentFixture<WizardSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
