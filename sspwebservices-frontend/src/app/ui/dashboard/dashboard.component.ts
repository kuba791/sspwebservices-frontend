import {Component, OnDestroy, OnInit} from '@angular/core';
import {DashboardService} from "../../service/dashboard/dashboard.service";
import {ActivatedRoute} from "@angular/router";
import {NotificationService} from "../../service/notification/notification.service";
import {DataService, NavigationResult} from "../../service/utilities/data.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  navigationResult: NavigationResult

  constructor(public dashboardService: DashboardService, public route: ActivatedRoute,
              public notificationService: NotificationService, public dataService: DataService) {
  }

  /**
   * Init method
   */
  ngOnInit() {

    this.navigationResult = this.dataService.navigationResult;

    if (this.navigationResult != undefined) {

      if (this.navigationResult.showSnack) {
        if (this.navigationResult.success) {
          this.notificationService.displayInfoSnack(this.navigationResult.message);
        }
        else {
          this.notificationService.displayErrorSnack(this.navigationResult.message);
        }
      }
    }
  }

  ngOnDestroy() {
    this.dataService.navigationResult = undefined;
  }
}
