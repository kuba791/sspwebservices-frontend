import {Component, OnInit} from '@angular/core';
import {User} from "../../../model/user/user";
import {UserPropertyKey} from "../../../model/user/user.property.key";
import {UserService} from "../../../service/user/user.service";
import {UserPropertyJson} from "../../../model/user/user.property.json";

@Component({
  selector: 'app-user-configuration',
  templateUrl: './user-configuration.component.html',
  styleUrls: ['./user-configuration.component.css']
})
export class UserConfigurationComponent implements OnInit {

  userId: number;
  user: User;
  userPropertyJson: UserPropertyJson;

  xenDesktopOrgUnitComputers: string;
  xenDesktopOrgUnitUsers: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userId = Number(sessionStorage.getItem('user_id'));
    this.getUserConfiguration();
  }

  getUserConfiguration() {
    console.log("Retrieving user properties");

    let jsonPromise = this.userService.getUserPropertyJson(this.userId);
  }

  /**
   * Saves the XenDesktop configuration of the current user.
   */
  onSaveXenDesktopConfiguration() {
    console.log("Saving user properties...")

    let properties = new Map();

    properties.set(UserPropertyKey.AD_ORG_UNIT_COMPUTERS, this.xenDesktopOrgUnitComputers);
    properties.set(UserPropertyKey.AD_ORG_UNIT_USERS, this.xenDesktopOrgUnitUsers);

    this.userService.saveUserProperties(this.userId, properties);
  }
}
