import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "../../../service/auth/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  logoPath = 'assets/images/fhswf_logo.png';

  registrationPage = 'registration';

  userMail = '';
  password = '';
  invalidateLogin = false;

  constructor(private router: Router, private loginService: AuthenticationService) {
  }

  ngOnInit() {
  }

  /**
   * Check if the entered credentials are valid.
   */
  checkLogin() {
    (this.loginService.authenticate(this.userMail, this.password).subscribe(
      data => {
        console.log("checkLogin was successful.")
        this.login();
      },
      error => {
        console.log("checkLogin failed.")
        this.invalidateLogin = true;
      }
    ));
  }

  /**
   * Logs the user in.
   */
  login() {
    this.loginService.login();
    this.invalidateLogin = false;
  }
}
