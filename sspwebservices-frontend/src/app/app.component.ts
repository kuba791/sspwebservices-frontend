import { Component } from '@angular/core';
import { AuthenticationService } from "./service/auth/authentication.service";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FH Südwestfalen - SSP';

  constructor(private auth: AuthenticationService, private http: HttpClient, private router: Router) {
    this.auth.authenticate(undefined, undefined);
  }

  logout() {
    this.http.post('logout', {}).pipe(
      finalize(() => {
        this.auth.authenticated = false;
        this.router.navigateByUrl('/login');
      })
    ).subscribe();
  }
}
