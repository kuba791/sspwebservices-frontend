import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './ui/dashboard/dashboard.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {HeaderComponent} from './ui/layout/header/header.component';
import {FooterComponent} from './ui/layout/footer/footer.component';
import {LoginComponent} from './ui/auth/login/login.component';
import {LogoutComponent} from './ui/auth/logout/logout.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {WebServiceComponent} from './ui/webservice/webService.component';
import {MachineCatalogWizardComponent} from './ui/wizard/machine-catalog-wizard/machine-catalog-wizard.component';
import {MatStepperModule} from "@angular/material/stepper";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSliderModule} from "@angular/material/slider";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatTreeModule} from "@angular/material/tree";
import {WizardSuccessComponent} from './ui/wizard/wizard-success/wizard-success.component';
import {WizardErrorComponent} from './ui/wizard/wizard-error/wizard-error.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {BasicAuthHttpInterceptorService} from "./service/auth/basic-auth-http-interceptor.service";
import {HttpClientService} from "./service/auth/http-client.service";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ListMachinesComponent} from './ui/webservice/machines/list-machines.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatTableModule} from "@angular/material/table";
import {MatDialogModule} from "@angular/material/dialog";
import {NotFoundComponent} from './ui/not-found/not-found.component';
import {MatSelectModule} from "@angular/material/select";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatRadioModule} from "@angular/material/radio";
import {UserRegistrationComponent} from './ui/registration/user-registration/user-registration.component';
import {MatMenuModule} from "@angular/material/menu";
import {UserConfigurationComponent} from './ui/settings/user-configuration/user-configuration.component';
import {LoggerModule, NgxLoggerLevel} from "ngx-logger";
import {DeliveryGroupWizardComponent} from './ui/wizard/delivery-group-wizard/delivery-group-wizard.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatPaginatorModule} from "@angular/material/paginator";
import {ErrorSnackBar, InfoSnackBar, SuccessSnackBar, WarnSnackBar} from "./service/notification/notification.service";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {DeleteMachinesDialogComponent} from './ui/layout/dialogs/delete-machines-dialog/delete-machines-dialog.component';
import {AssignedUsersDialogComponent} from './ui/layout/dialogs/assigned-users-dialog/assigned-users-dialog.component';
import {UserAssignmentDialogComponent} from './ui/layout/dialogs/user-assignment-dialog/user-assignment-dialog.component';
import {PerformActionOnMachineDialogComponent} from './ui/layout/dialogs/perform-action-on-machine-dialog/perform-action-on-machine-dialog.component';
import {AddMachinesToMachineCatalogDialogComponent} from "./ui/layout/dialogs/add-machines-to-machine-catalog-dialog/add-machines-to-machine-catalog-dialog.component";
import { AddMachinesToDeliveryGroupDialogComponent } from './ui/layout/dialogs/add-machines-to-delivery-group-dialog/add-machines-to-delivery-group-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    LogoutComponent,
    WebServiceComponent,
    MachineCatalogWizardComponent,
    WizardSuccessComponent,
    WizardErrorComponent,
    ListMachinesComponent,
    NotFoundComponent,
    UserRegistrationComponent,
    UserConfigurationComponent,
    DeliveryGroupWizardComponent,
    UserAssignmentDialogComponent,
    AssignedUsersDialogComponent,
    PerformActionOnMachineDialogComponent,
    DeleteMachinesDialogComponent,
    AddMachinesToMachineCatalogDialogComponent,
    AddMachinesToDeliveryGroupDialogComponent,
    InfoSnackBar,
    SuccessSnackBar,
    WarnSnackBar,
    ErrorSnackBar,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSliderModule,
    MatInputModule,
    FormsModule,
    MatListModule,
    MatTreeModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatExpansionModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule,
    MatTooltipModule,
    MatRadioModule,
    MatMenuModule,
    LoggerModule.forRoot({
      serverLoggingUrl: 'api/logs',
      level: NgxLoggerLevel.TRACE,
      serverLogLevel: NgxLoggerLevel.ERROR,
      disableConsoleLogging: false
    }),
    MatSidenavModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSnackBarModule
  ],
  entryComponents: [ListMachinesComponent,
    UserAssignmentDialogComponent,
    AssignedUsersDialogComponent,
    PerformActionOnMachineDialogComponent,
    DeleteMachinesDialogComponent,
    AddMachinesToMachineCatalogDialogComponent,
    AddMachinesToDeliveryGroupDialogComponent,
    InfoSnackBar,
    SuccessSnackBar,
    WarnSnackBar,
    ErrorSnackBar,
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass:BasicAuthHttpInterceptorService, multi:true},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 5000, horizontalPosition: "right", verticalPosition: "top"}},
    HttpClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
