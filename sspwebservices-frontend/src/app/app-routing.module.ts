import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from "./ui/dashboard/dashboard.component";
import {WebServiceComponent} from "./ui/webservice/webService.component";
import {MachineCatalogWizardComponent} from "./ui/wizard/machine-catalog-wizard/machine-catalog-wizard.component";
import {LoginComponent} from "./ui/auth/login/login.component";
import {LogoutComponent} from "./ui/auth/logout/logout.component";
import {AuthGuardService} from "./service/auth/auth-guard.service";
import {ListMachinesComponent} from "./ui/webservice/machines/list-machines.component";
import {UserRegistrationComponent} from "./ui/registration/user-registration/user-registration.component";
import {UserConfigurationComponent} from "./ui/settings/user-configuration/user-configuration.component";
import {DeliveryGroupWizardComponent} from "./ui/wizard/delivery-group-wizard/delivery-group-wizard.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'dashboard'},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuardService]},
  {path: 'registration', component: UserRegistrationComponent},
  {path: 'web-services', component: WebServiceComponent, canActivate: [AuthGuardService]},
  {path: 'web-services/machine-catalogs', component: MachineCatalogWizardComponent, canActivate: [AuthGuardService]},
  // {path: 'web-service/machine-catalog/error', component: WizardErrorComponent, canActivate: [AuthGuardService]},
  // {path: 'web-service/machine-catalog/success', component: WizardSuccessComponent, canActivate: [AuthGuardService]},
  {path: 'web-services/delivery-groups', component: DeliveryGroupWizardComponent, canActivate: [AuthGuardService]},
  {path: 'machines', component: ListMachinesComponent, canActivate: [AuthGuardService]},
  {path: 'settings/xendesktop', component: UserConfigurationComponent, canActivate: [AuthGuardService]},
  {path: '**', redirectTo: '/404', canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
