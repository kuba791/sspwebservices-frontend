// import { Injectable } from '@angular/core';
// import { Observable, Subject, Subscription, BehaviorSubject } from "rxjs";
//
// @Injectable({
//   providedIn: 'root'
// })
// export class SessionTimeoutService {
//
//   private count: number = 0;
//   private serviceId: string = 'sessiontTimeoutService-' + Math.floor(Math.random() * 10000);
//   private timeoutSeconds: number = 5;
//   private timerSubscription: Subscription;
//   private timer: Observable<number>;
//   private resetOnTrigger: boolean = false;
//
//   public timeoutExpired: Subject<number> = new Subject<number>();
//
//   constructor() {
//
//     console.log("Constructed sessionTimeOutService" + this.serviceId);
//
//     this.timeoutExpired.subscribe(next => {
//       console.log("timeoutExpired subject next... " + next.toString())
//     });
//     this.startTimer();
//   }
//
//   public startTimer() {
//
//     if (this.timerSubscription) {
//       this.timerSubscription.unsubscribe();
//     }
//
//     this.timer = Observable.timer(this.timeoutSeconds * 1000);
//     this.timerSubscription = this.timer.subscribe(num => {
//       this.timerComplete(num);
//     });
//   }
//
//   public stopTimer() {
//       this.timerSubscription.unsubscribe();
//   }
//
//   public resetTimer() {
//
//     if (this.timerSubscription) {
//       this.timerSubscription.unsubscribe();
//     }
//
//     this.timer = Observable.timer(this.timeoutSeconds * 1000);
//     this.timerSubscription = this.timer.subscribe(num => {
//       this.timerComplete(num);
//     });
//   }
//
//   private timerComplete(num: number) {
//
//     this.timeoutExpired.next(++this.count);
//
//     if (this.resetOnTrigger) {
//       this.startTimer();
//     }
//   }
//
// }
