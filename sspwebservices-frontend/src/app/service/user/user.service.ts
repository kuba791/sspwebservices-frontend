import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {User} from "../../model/user/user";
import {NGXLogger} from "ngx-logger";
import {UserPropertyJson} from "../../model/user/user.property.json";

const USER_BASE_API_URL = "/users/";
const REGISTER_USER_URL = "/register";
const API_USER_PROPERTIES = "/properties";
const API_SETTINGS_ROOT_URL = "/settings";
const API_SETTINGS_XENDESKTOP_URL = "/xendesktop";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userPropertyJson: UserPropertyJson;

  constructor(private http: HttpClient, private router: Router, private logger: NGXLogger) {
  }

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  /**
   * Registers a new user.
   * @param userMail
   * @param userPassword
   */
  registerUser(userMail: string, userPassword: string) {
    this.logger.debug("User service will request backend")

    this.http.post<Observable<User>>(USER_BASE_API_URL + REGISTER_USER_URL, {
      userMail: userMail,
      userPassword: userPassword
    }, {headers: this.headers})
      .subscribe(user => {

        if (user != null) {
          this.router.navigate(['login']);
        } else {
          alert("Registration failed.");
        }
      });
  }

  /**
   * Delivers the user properties to the backend to be saved in the db.
   *
   * @param userId
   * @param properties {@link Map}
   */
  saveUserProperties(userId: number, properties: Map<string, string>) {
    this.logger.debug("saveUserProperties()")
    const convertedMap = {};

    properties.forEach((value: string, key: string) => {
      convertedMap[key] = value;
    });

    this.http.post<User>(API_USER_PROPERTIES,
      {user_id: userId, properties: convertedMap},
      {headers: this.headers})
      .subscribe(user => {
        if (user != null) {
          alert("Saving users properties was successful!");
        } else {
          alert("Something went wrong while saving users properties.");
        }
      });
  }

  /**
   * Get all properties of given user.
   *
   * @param userId
   */
  getUserPropertyJson(userId: number): Promise<UserPropertyJson> {

    return this.http.get<UserPropertyJson>(USER_BASE_API_URL + {userId} + API_USER_PROPERTIES,{headers: this.headers})
      .toPromise()
      .then(json => {
        console.log(json);

        this.userPropertyJson.userId = json.userId;
        this.userPropertyJson.properties = json.properties;

        return json;
      });

  }
}
