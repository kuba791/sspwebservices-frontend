import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private router: Router) { }

  /**
   * Redirects to the list of available web service to order
   */
  navigateToWebServicesList() {
    return this.router.navigate(['web-services']);
  }

  /**
   * Redirects to users created delivery groups
   */
  navigateToUsersMachines() {
    return this.router.navigate(['machines']);
  }
}
