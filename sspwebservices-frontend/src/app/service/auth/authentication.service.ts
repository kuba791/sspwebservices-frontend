import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {BehaviorSubject} from "rxjs";
import {Router} from "@angular/router";
import {User} from "../../model/user/user";
import {UserService} from "../user/user.service";
import {HeaderComponent} from "../../ui/layout/header/header.component";

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  baseUrl = environment.baseUrl;
  authenticated = false;

  private loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable());

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  /**
   * Authenticates the incoming user.
   * @param userMail - Mail address of the current user
   * @param password - Password of the current user
   */
  authenticate(userMail: string, password: string) {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(userMail + ':' + password)});

    let url = '/authenticate';

    return this.httpClient.get<User>(url, {headers}).pipe(
      map(
        userData => {
          // Store authentication token in the session storage to check if the user is authenticated or not
          this.setUserInformationInSessionStorage(userData, password);
          return userData;
        }
      )
    );
  }

  /**
   * Stores users information in current session storage.
   * @param userData - Current user object
   * @param password - Password of the user for authorized rest calls
   */
  setUserInformationInSessionStorage(userData: User, password: string) {
    sessionStorage.setItem('user_mail', userData.mail);
    sessionStorage.setItem('user_id', userData.id.toString());
    let authString = 'Basic ' + btoa(userData.mail + ':' + password);
    sessionStorage.setItem('basic_auth', authString);
  }

  /**
   * Navigates the current user to the dashboard if the authentication was successful.
   */
  login() {
    this.loggedIn.next(true);
    this.router.navigateByUrl('dashboard').then(r => undefined);
  }

  /**
   * Checks if a user is currently logged in.
   */
  private tokenAvailable(): boolean {
    let user = sessionStorage.getItem('user_mail');
    return !(user === null);
  }

  /**
   * Logs the current user out.
   */
  logout() {
    console.log("User will be logged out.")
    sessionStorage.clear();
    this.loggedIn.next(false);
  }
}
