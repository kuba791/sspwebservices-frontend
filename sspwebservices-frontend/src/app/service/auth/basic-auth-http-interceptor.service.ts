import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BasicAuthHttpInterceptorService implements HttpInterceptor {

  baseUrl = environment.baseUrl;

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (sessionStorage.getItem('user_mail') && sessionStorage.getItem('basic_auth')) {
      req = req.clone({
        setHeaders: {
          Authorization: sessionStorage.getItem('basic_auth')
        }
      })
    }

    const apiRequest = req.clone({url: `${this.baseUrl}${req.url}`});
    return next.handle(apiRequest);
  }
}
