import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {WebService} from "../../model/webservice/webService";
import {Router} from "@angular/router";
import {MachineCatalog} from "../../model/webservice/citrix/machinecatalog/machine-catalog";
import {Observable} from "rxjs";
import {DeliveryGroup} from "../../model/webservice/citrix/deliverygroup/delivery-group";
import {ActiveDirectoryUser} from "../../model/user/active.directory.user";
import {Machine} from "../../model/webservice/citrix/machine/machine";
import {PowerAction} from "../../model/webservice/citrix/machine/power-action";

const URL_WEB_SERVICES_ROOT: string = "/web-services";
const URL_DASHBOARD: string = "/dashboard";
const URL_WS_MACHINE_CATALOGS: string = URL_WEB_SERVICES_ROOT + "/machine-catalogs";
const URL_WS_DELIVERY_GROUPS: string = URL_WEB_SERVICES_ROOT + "/delivery-groups";

const API_ALL: string = "/all";
const API_WEB_SERVICES_ROOT: string = "/web-services";
const API_WS_MACHINE_CATALOGS: string = "/machine-catalogs";
const API_WS_MACHINES: string = "/machines";
const API_WS_DELIVERY_GROUPS: string = "/delivery-groups";
const API_AD_USERS: string = "/active-directory-users"

export class PowerShellStatus {
  statusCode: number;
  description: string;
}

export class PowerShellResponse {
  result: number;
  powerShellStatus: PowerShellStatus;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class WebServiceService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private router: Router) { }

  /**
   * Method to retrieve all available web services for the user to book.
   */
  getAllAvailableXenDesktopComponents() {
    return this.http.get<WebService[]>(URL_WEB_SERVICES_ROOT + API_ALL, {headers: this.headers});
  }

  /**
   * Navigates the user the wizard of the correct type.
   * @param type
   */
  onOrderingSpecificService(type: string) {

    // Navigate to Wizards for Citrix XenDesktop machine catalog
    if (type === "xendesktop_machine_catalog") {
      return this.router.navigate([URL_WS_MACHINE_CATALOGS]);
    }
    // Navigate to Wizards for Citrix XenDesktop delivery group
    else if (type === "xendesktop_delivery_group") {
      return this.router.navigate([URL_WS_DELIVERY_GROUPS]);
    }
    // Else stay on current page
    else {
      alert("Sorry, this web service has not been implemented yet.")
      return "";
    }
  }

  /**
   * Registers the configured web service in the backend (angular) application.
   * @param userId ID of the user to which the machine catalog should be assigned to.
   * @param catalog Holds all the information about the machine catalog to be created.
   */
  createMachineCatalog(userId: number, catalog: MachineCatalog): Observable<MachineCatalog> {

    return this.http.post<MachineCatalog>( "/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINE_CATALOGS,
      {catalog: catalog},
      {headers: this.headers});
  }

  /**
   * Navigates the user back to the dashboard after processing any order.
   * @param success Param used for showing a positive or negative info notification
   * @param message Message used to be shown in the snackbar after the process.
   */
  navigateToDashboard() {

    return this.router.navigate([URL_DASHBOARD]);
  }

  /**
   * Requests all active directory users of the currently logged in user.
   * @param userId
   */
  retrieveActiveDirectoryUsersOfCurrentUser(userId: number) {
    return this.http.get<ActiveDirectoryUser[]>("/" + userId + API_AD_USERS,
      {headers: this.headers});
  }

  /**
   * Requests all machine catalogs of the currently logged in user.
   * @param userId
   */
  retrieveAllMachineCatalogsOfCurrentUser(userId: number) {
    return this.http.get<MachineCatalog[]>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINE_CATALOGS,
      {headers: this.headers});
  }

  /**
   * Requests all available machines of the selected machine catalog
   * @param userId ID of the user and owner of the catalog
   * @param catalogUid UID of the requested catalog
   */
  retrieveAllAvailableMachinesOfCatalog(userId: number, catalogUid: number) {
    return this.http.get<Machine[]>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINE_CATALOGS + "/"
      + catalogUid + API_WS_MACHINES,
      {headers: this.headers});
  }

  /**
   * Requests all available delivery groups of the current user.
   * @param userId ID of the user and owner of the delivery groups
   */
  retrieveAllDeliveryGroupsOfCurrentUser(userId: number) {
    return this.http.get<DeliveryGroup[]>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_DELIVERY_GROUPS,
      {headers: this.headers});
  }

  /**
   * Posts information for creating a new delivery group
   * @param userId ID used for assigning delivery group to user
   * @param deliveryGroup Holds all needed information about the delivery group to create.
   */
  createDeliveryGroup(userId: number, deliveryGroup: DeliveryGroup) {
    return this.http.post<DeliveryGroup>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_DELIVERY_GROUPS,
      {deliveryGroup: deliveryGroup},
      {headers: this.headers});
  }

  /**
   * Lists all machines of every delivery group of the current user.
   * @param userId ID of the current user.
   */
  retrieveAllMachinesOfUser(userId: number) {
    return this.http.get<Machine[]>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINES,
      {headers: this.headers});
  }

  /**
   * Sends the selected power action to the backend to execute it on the selected machine.
   * @param userId
   * @param machineName
   * @param action
   */
  performActionOnMachine(userId: number, machineName: string, action: PowerAction) {

    console.log("Action: '" + action + "' will be performed on: '" + machineName + "'")

    return this.http.post<PowerShellResponse>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINES,
      {"machine_name": machineName, "action": action.toString()},
      {headers: this.headers});
  }

  /**
   * Adds new machines to a specific machine catalog.
   * @param userId
   * @param machineCatalogName
   * @param machineCount
   */
  addNewMachinesToMachineCatalog(userId: number, machineCatalogName: string, machineCount: number) {
    return this.http.put<PowerShellResponse>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINE_CATALOGS + "/add-machines",
      {"catalogName": machineCatalogName, "count": machineCount.toString()},
      {headers: this.headers});
  }

  /**
   * Sends the command to delete the selected machines.
   * @param userId
   * @param machines
   */
  deleteSelectedMachines(userId: number, machines: Machine[]) {
    return this.http.post<PowerShellResponse>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINES + "/deletes",
      {"machines": machines},
      {headers: this.headers});
  }

  /**
   * Re-assigns user(s) to selected machine.
   * @param userId
   * @param machines
   */
  assignUsersToMachine(userId: number, machines: Machine[]) {
    return this.http.put<PowerShellResponse>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_MACHINES + "/assign-users",
      {"machines": machines},
      {headers: this.headers});
  }

  /**
   * Adds machine(s) to the selected delivery group.
   * @param userId
   * @param machines
   */
  addMachinesToDeliveryGroup(userId: number, machines: Machine[]) {
    return this.http.put<PowerShellResponse>("/" + userId + API_WEB_SERVICES_ROOT + API_WS_DELIVERY_GROUPS + "/add-machines",
      {"machines": machines},
      {headers: this.headers});
  }
}
