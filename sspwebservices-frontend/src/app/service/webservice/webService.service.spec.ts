import { TestBed } from '@angular/core/testing';

import { WebServiceService } from './webService.service';

describe('WebserviceListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebServiceService = TestBed.get(WebServiceService);
    expect(service).toBeTruthy();
  });
});
