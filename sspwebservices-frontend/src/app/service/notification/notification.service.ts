import {Component, Inject, Injectable} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  constructor(public snackBar: MatSnackBar) {
  }

  displayInfoSnack(message: string) {
    this.snackBar.openFromComponent(InfoSnackBar, {
      data: message
    });
  }

  displaySuccessSnack(message: string) {
    this.snackBar.openFromComponent(SuccessSnackBar, {
      data: message
    });
  }

  displayWarnSnack(message: string) {
    this.snackBar.openFromComponent(WarnSnackBar, {
      data: message
    });
  }

  displayErrorSnack(message: string) {
    this.snackBar.openFromComponent(ErrorSnackBar, {
      data: message
    });
  }

}

@Component({
  selector: 'info-snackbar',
  templateUrl: './snackbars/info-snackbar.html',
  styleUrls: ['./snackbars/info-snackbar.css']
})
export class InfoSnackBar {

  message: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string) {
    this.message = this.data;
  }
}

@Component({
  selector: 'success-snackbar',
  templateUrl: './snackbars/success-snackbar.html',
  styleUrls: ['./snackbars/success-snackbar.css']
})
export class SuccessSnackBar {

  message: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string) {
    this.message = this.data;
  }
}

@Component({
  selector: 'error-snackbar',
  templateUrl: './snackbars/error-snackbar.html',
  styleUrls: ['./snackbars/error-snackbar.css']
})
export class ErrorSnackBar {

  message: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string) {
    this.message = this.data;
  }
}

@Component({
  selector: 'warn-snackbar',
  templateUrl: './snackbars/warn-snackbar.html',
  styleUrls: ['./snackbars/warn-snackbar.css']
})
export class WarnSnackBar {
  message: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string) {
    this.message = this.data;
  }
}
