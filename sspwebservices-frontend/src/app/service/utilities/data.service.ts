import {Injectable} from '@angular/core';

export interface NavigationResult {
  showSnack: boolean,
  success: boolean,
  message: string
}

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public navigationResult: NavigationResult;
  public machineCatalog: string;
}
